package com.mck.service.dryaccount.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mck.service.dryaccount.domain.DWBankAccount;
import com.mck.service.dryaccount.repository.DWBankAccountRepository;
import com.mck.service.dryaccount.web.rest.errors.BadRequestAlertException;
import com.mck.service.dryaccount.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DWBankAccount.
 */
@RestController
@RequestMapping("/api")
public class DWBankAccountResource {

    private final Logger log = LoggerFactory.getLogger(DWBankAccountResource.class);

    private static final String ENTITY_NAME = "dWBankAccount";

    private final DWBankAccountRepository dWBankAccountRepository;

    public DWBankAccountResource(DWBankAccountRepository dWBankAccountRepository) {
        this.dWBankAccountRepository = dWBankAccountRepository;
    }

    /**
     * POST  /dw-bank-accounts : Create a new dWBankAccount.
     *
     * @param dWBankAccount the dWBankAccount to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dWBankAccount, or with status 400 (Bad Request) if the dWBankAccount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dw-bank-accounts")
    @Timed
    public ResponseEntity<DWBankAccount> createDWBankAccount(@Valid @RequestBody DWBankAccount dWBankAccount) throws URISyntaxException {
        log.debug("REST request to save DWBankAccount : {}", dWBankAccount);
        if (dWBankAccount.getId() != null) {
            throw new BadRequestAlertException("A new dWBankAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DWBankAccount result = dWBankAccountRepository.save(dWBankAccount);
        return ResponseEntity.created(new URI("/api/dw-bank-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dw-bank-accounts : Updates an existing dWBankAccount.
     *
     * @param dWBankAccount the dWBankAccount to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dWBankAccount,
     * or with status 400 (Bad Request) if the dWBankAccount is not valid,
     * or with status 500 (Internal Server Error) if the dWBankAccount couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dw-bank-accounts")
    @Timed
    public ResponseEntity<DWBankAccount> updateDWBankAccount(@Valid @RequestBody DWBankAccount dWBankAccount) throws URISyntaxException {
        log.debug("REST request to update DWBankAccount : {}", dWBankAccount);
        if (dWBankAccount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DWBankAccount result = dWBankAccountRepository.save(dWBankAccount);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dWBankAccount.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dw-bank-accounts : get all the dWBankAccounts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dWBankAccounts in body
     */
    @GetMapping("/dw-bank-accounts")
    @Timed
    public List<DWBankAccount> getAllDWBankAccounts() {
        log.debug("REST request to get all DWBankAccounts");
        return dWBankAccountRepository.findAll();
    }

    /**
     * GET  /dw-bank-accounts/:id : get the "id" dWBankAccount.
     *
     * @param id the id of the dWBankAccount to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dWBankAccount, or with status 404 (Not Found)
     */
    @GetMapping("/dw-bank-accounts/{id}")
    @Timed
    public ResponseEntity<DWBankAccount> getDWBankAccount(@PathVariable Long id) {
        log.debug("REST request to get DWBankAccount : {}", id);
        Optional<DWBankAccount> dWBankAccount = dWBankAccountRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(dWBankAccount);
    }

    /**
     * DELETE  /dw-bank-accounts/:id : delete the "id" dWBankAccount.
     *
     * @param id the id of the dWBankAccount to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dw-bank-accounts/{id}")
    @Timed
    public ResponseEntity<Void> deleteDWBankAccount(@PathVariable Long id) {
        log.debug("REST request to delete DWBankAccount : {}", id);

        dWBankAccountRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
