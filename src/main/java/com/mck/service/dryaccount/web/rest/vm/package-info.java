/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mck.service.dryaccount.web.rest.vm;
