package com.mck.service.dryaccount.repository;

import com.mck.service.dryaccount.domain.DWBankAccount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DWBankAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DWBankAccountRepository extends JpaRepository<DWBankAccount, Long> {

}
