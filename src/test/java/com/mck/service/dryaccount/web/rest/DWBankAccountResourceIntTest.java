package com.mck.service.dryaccount.web.rest;

import com.mck.service.dryaccount.DryaccountApp;

import com.mck.service.dryaccount.domain.DWBankAccount;
import com.mck.service.dryaccount.repository.DWBankAccountRepository;
import com.mck.service.dryaccount.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mck.service.dryaccount.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DWBankAccountResource REST controller.
 *
 * @see DWBankAccountResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DryaccountApp.class)
public class DWBankAccountResourceIntTest {

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PAN_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PAN_NUMBER = "BBBBBBBBBB";

    @Autowired
    private DWBankAccountRepository dWBankAccountRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDWBankAccountMockMvc;

    private DWBankAccount dWBankAccount;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DWBankAccountResource dWBankAccountResource = new DWBankAccountResource(dWBankAccountRepository);
        this.restDWBankAccountMockMvc = MockMvcBuilders.standaloneSetup(dWBankAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DWBankAccount createEntity(EntityManager em) {
        DWBankAccount dWBankAccount = new DWBankAccount()
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .phone(DEFAULT_PHONE)
            .address(DEFAULT_ADDRESS)
            .postalCode(DEFAULT_POSTAL_CODE)
            .panNumber(DEFAULT_PAN_NUMBER);
        return dWBankAccount;
    }

    @Before
    public void initTest() {
        dWBankAccount = createEntity(em);
    }

    @Test
    @Transactional
    public void createDWBankAccount() throws Exception {
        int databaseSizeBeforeCreate = dWBankAccountRepository.findAll().size();

        // Create the DWBankAccount
        restDWBankAccountMockMvc.perform(post("/api/dw-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWBankAccount)))
            .andExpect(status().isCreated());

        // Validate the DWBankAccount in the database
        List<DWBankAccount> dWBankAccountList = dWBankAccountRepository.findAll();
        assertThat(dWBankAccountList).hasSize(databaseSizeBeforeCreate + 1);
        DWBankAccount testDWBankAccount = dWBankAccountList.get(dWBankAccountList.size() - 1);
        assertThat(testDWBankAccount.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testDWBankAccount.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testDWBankAccount.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testDWBankAccount.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testDWBankAccount.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testDWBankAccount.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testDWBankAccount.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testDWBankAccount.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testDWBankAccount.getPanNumber()).isEqualTo(DEFAULT_PAN_NUMBER);
    }

    @Test
    @Transactional
    public void createDWBankAccountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dWBankAccountRepository.findAll().size();

        // Create the DWBankAccount with an existing ID
        dWBankAccount.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDWBankAccountMockMvc.perform(post("/api/dw-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWBankAccount)))
            .andExpect(status().isBadRequest());

        // Validate the DWBankAccount in the database
        List<DWBankAccount> dWBankAccountList = dWBankAccountRepository.findAll();
        assertThat(dWBankAccountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUsernameIsRequired() throws Exception {
        int databaseSizeBeforeTest = dWBankAccountRepository.findAll().size();
        // set the field null
        dWBankAccount.setUsername(null);

        // Create the DWBankAccount, which fails.

        restDWBankAccountMockMvc.perform(post("/api/dw-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWBankAccount)))
            .andExpect(status().isBadRequest());

        List<DWBankAccount> dWBankAccountList = dWBankAccountRepository.findAll();
        assertThat(dWBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = dWBankAccountRepository.findAll().size();
        // set the field null
        dWBankAccount.setPassword(null);

        // Create the DWBankAccount, which fails.

        restDWBankAccountMockMvc.perform(post("/api/dw-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWBankAccount)))
            .andExpect(status().isBadRequest());

        List<DWBankAccount> dWBankAccountList = dWBankAccountRepository.findAll();
        assertThat(dWBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = dWBankAccountRepository.findAll().size();
        // set the field null
        dWBankAccount.setFirstName(null);

        // Create the DWBankAccount, which fails.

        restDWBankAccountMockMvc.perform(post("/api/dw-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWBankAccount)))
            .andExpect(status().isBadRequest());

        List<DWBankAccount> dWBankAccountList = dWBankAccountRepository.findAll();
        assertThat(dWBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = dWBankAccountRepository.findAll().size();
        // set the field null
        dWBankAccount.setLastName(null);

        // Create the DWBankAccount, which fails.

        restDWBankAccountMockMvc.perform(post("/api/dw-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWBankAccount)))
            .andExpect(status().isBadRequest());

        List<DWBankAccount> dWBankAccountList = dWBankAccountRepository.findAll();
        assertThat(dWBankAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDWBankAccounts() throws Exception {
        // Initialize the database
        dWBankAccountRepository.saveAndFlush(dWBankAccount);

        // Get all the dWBankAccountList
        restDWBankAccountMockMvc.perform(get("/api/dw-bank-accounts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dWBankAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE.toString())))
            .andExpect(jsonPath("$.[*].panNumber").value(hasItem(DEFAULT_PAN_NUMBER.toString())));
    }
    

    @Test
    @Transactional
    public void getDWBankAccount() throws Exception {
        // Initialize the database
        dWBankAccountRepository.saveAndFlush(dWBankAccount);

        // Get the dWBankAccount
        restDWBankAccountMockMvc.perform(get("/api/dw-bank-accounts/{id}", dWBankAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dWBankAccount.getId().intValue()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE.toString()))
            .andExpect(jsonPath("$.panNumber").value(DEFAULT_PAN_NUMBER.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingDWBankAccount() throws Exception {
        // Get the dWBankAccount
        restDWBankAccountMockMvc.perform(get("/api/dw-bank-accounts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDWBankAccount() throws Exception {
        // Initialize the database
        dWBankAccountRepository.saveAndFlush(dWBankAccount);

        int databaseSizeBeforeUpdate = dWBankAccountRepository.findAll().size();

        // Update the dWBankAccount
        DWBankAccount updatedDWBankAccount = dWBankAccountRepository.findById(dWBankAccount.getId()).get();
        // Disconnect from session so that the updates on updatedDWBankAccount are not directly saved in db
        em.detach(updatedDWBankAccount);
        updatedDWBankAccount
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .address(UPDATED_ADDRESS)
            .postalCode(UPDATED_POSTAL_CODE)
            .panNumber(UPDATED_PAN_NUMBER);

        restDWBankAccountMockMvc.perform(put("/api/dw-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDWBankAccount)))
            .andExpect(status().isOk());

        // Validate the DWBankAccount in the database
        List<DWBankAccount> dWBankAccountList = dWBankAccountRepository.findAll();
        assertThat(dWBankAccountList).hasSize(databaseSizeBeforeUpdate);
        DWBankAccount testDWBankAccount = dWBankAccountList.get(dWBankAccountList.size() - 1);
        assertThat(testDWBankAccount.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testDWBankAccount.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testDWBankAccount.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testDWBankAccount.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testDWBankAccount.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testDWBankAccount.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testDWBankAccount.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testDWBankAccount.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testDWBankAccount.getPanNumber()).isEqualTo(UPDATED_PAN_NUMBER);
    }

    @Test
    @Transactional
    public void updateNonExistingDWBankAccount() throws Exception {
        int databaseSizeBeforeUpdate = dWBankAccountRepository.findAll().size();

        // Create the DWBankAccount

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDWBankAccountMockMvc.perform(put("/api/dw-bank-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWBankAccount)))
            .andExpect(status().isBadRequest());

        // Validate the DWBankAccount in the database
        List<DWBankAccount> dWBankAccountList = dWBankAccountRepository.findAll();
        assertThat(dWBankAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDWBankAccount() throws Exception {
        // Initialize the database
        dWBankAccountRepository.saveAndFlush(dWBankAccount);

        int databaseSizeBeforeDelete = dWBankAccountRepository.findAll().size();

        // Get the dWBankAccount
        restDWBankAccountMockMvc.perform(delete("/api/dw-bank-accounts/{id}", dWBankAccount.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DWBankAccount> dWBankAccountList = dWBankAccountRepository.findAll();
        assertThat(dWBankAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DWBankAccount.class);
        DWBankAccount dWBankAccount1 = new DWBankAccount();
        dWBankAccount1.setId(1L);
        DWBankAccount dWBankAccount2 = new DWBankAccount();
        dWBankAccount2.setId(dWBankAccount1.getId());
        assertThat(dWBankAccount1).isEqualTo(dWBankAccount2);
        dWBankAccount2.setId(2L);
        assertThat(dWBankAccount1).isNotEqualTo(dWBankAccount2);
        dWBankAccount1.setId(null);
        assertThat(dWBankAccount1).isNotEqualTo(dWBankAccount2);
    }
}
